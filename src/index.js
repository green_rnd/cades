import "./cadesplugin_api";
import ESignProvider from "./ESignProvider";

let provider;

async function init() {
    provider = new ESignProvider();
    await provider.init();
    await provider.updateCertificates();
    const certificates = provider.certificates;
  
    console.log(provider);
  }
  
setTimeout(init, 3000);